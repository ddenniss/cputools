#lang racket

(require "../../_lib_links/odysseus_all.rkt")
(require "../../_lib_links/odysseus_tabtree.rkt")
(require "../../_lib_links/odysseus_scrap.rkt")
(require "../../_lib_links/odysseus_report.rkt")
(require "../../_lib_links/odysseus_whereami.rkt")
(require "../../_lib_links/settings.rkt")

(require "../_lib/globals.rkt")
(require "../_lib/functions.rkt")
(require "../_lib/page_snippets.rkt")

(persistent h-user-wall-posts)
(persistent h-user-wall-posts-new)
(persistent uid-closed-wall)
(persistent uids-selected)

(define (update-cache uids-selected)
  (let* ((all-uids (uids-selected))
        (scanned-uids (hash-keys (h-user-wall-posts)))
        (closed-uids (uid-closed-wall))
        (non-scanned-uids (minus all-uids (append scanned-uids closed-uids)))
        )
    (cache-posts-by-uid
        #:uids non-scanned-uids
        #:cache-to h-user-wall-posts-new
        #:closed-to uid-closed-wall
        #:read-depth 30
        #:cache-write-frequency 50
        #:request-limit 2000
        )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define users-done (length (hash-keys (h-user-wall-posts))))
(define users-closed-done (length (uid-closed-wall)))
(define users-to-go (- (length (uids-selected)) users-done users-closed-done))

(--- (format "~a: Считываем посты со стен пользователей" (timestamp)))
(--- (format "Пользователей уже считано: ~a, закрытых пользователей: ~a, осталось считать: ~a" users-done users-closed-done users-to-go))

(update-cache uids-selected)

(--- (format "~a: Конец считывания постов" (timestamp)))
