<!DOCTYPE html>
<html lang="ru">
  <head>
    <!-- %%(str "timestamp: " (timestamp))%% -->
    %%(define page (@id page-id PAGES))%%
    %%(standard-head-part
        #:title ($ title page)
        #:description ($ description page)
        #:keywords ($ keywords page)
    )%%

  </head>
  <body>
    <div class="container">
      %%(print-header)%%

      %%(str post_cards)%%

    </div>
    %%(footer)%%
  </body>
</html>
